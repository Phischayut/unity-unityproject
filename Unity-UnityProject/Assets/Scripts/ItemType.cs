using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Phischayut.GameDev3.Basic
{
    public enum ItemType
    {
        COIN,
        BIGCOIN,
        POWERUP,
        POWERDOWN,
    }

}
